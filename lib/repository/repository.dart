import 'package:block_pattern/helper/helper.dart';
import 'package:block_pattern/model/login_response.dart';
import 'package:block_pattern/model/recipe_item_response/RecipeItemResponse.dart';
import 'package:block_pattern/model/recipe_response.dart';
import 'package:dio/dio.dart';

class RecipeRepository{
  final String apiKey = "E8C6397D83908300B4D54023402BB3387981A2A600E5D148B079DFD40CB56C506241096EDDF56397";
  static String mainUrl = "http://18.189.146.73/api/v1";

  final Dio _dio=Dio();
  var getRecipeUrl = '$mainUrl/recipe/list?';
  var getloginUrl = '$mainUrl/login?';
  var getrecipeUrl = '$mainUrl/recipe/items?';

 Future<RecipeResponse> getRecipes(String auth_token) async{
   var params = {'auth_token': auth_token,'location_id': 1};
   try{
     Response response=await _dio.post(getRecipeUrl,data: params);// queryParameters for get method
     print('Recipe Info: ${response.data}');
     return RecipeResponse.fromJson(response.data);
   }catch(error, stacktrace){
     print("Exception occured: $error stackTrace: $stacktrace");
     return RecipeResponse.withError("$error");
   }

  }
  Future<LoginResponse?> getLogin(String email,String pwd) async{
    LoginResponse? res;
    var params = {'email': email,'password': pwd};
    try{
      Response response=await _dio.post(getloginUrl,data: params);// queryParameters for get method
      print('User Info: ${response.data}');
      //Helper().showToast("" + response.m);
      res=LoginResponse.fromJson(response.data);

    }on DioError catch (e){
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        //print(e.response.data);
       // print(e.response.headers);

      } else {
        // Error due to setting up or sending the request
        print('Error sending request!');
        print(e.message);
      }
    }
    return res;
  }
  Future<LoginResponse?> getLogin2(String email,String pwd) async{
    LoginResponse? res;
    var params = {'email': email,'password': pwd};
    try{
      Response response=await _dio.post(getloginUrl,data: params);// queryParameters for get method
      print('User Info: ${response.data}');
      //Helper().showToast("" + response.m);
      res=LoginResponse.fromJson(response.data);

    }on DioError catch (e){
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
       // print(e.response.data);
       // print(e.response.headers);

      } else {
        // Error due to setting up or sending the request
        print('Error sending request!');
        print(e.message);
      }
    }
    return res;
  }

  Future<RecipeItemResponse?> getRecipeItem(String auth_token,String location_id,String recipe_id) async{
    RecipeItemResponse? res;
    var params = {'auth_token': auth_token,'location_id': location_id,'recipe_id':recipe_id};
    try{
      Response response=await _dio.post(getrecipeUrl,data: params);// queryParameters for get method
      print('Recipe item Info: ${response.data}');
      //Helper().showToast("" + response.m);
      res=RecipeItemResponse.fromJson(response.data);

    }on DioError catch (e){
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
       // print(e.response.data);
       // print(e.response.headers);

      } else {
        // Error due to setting up or sending the request
        print('Error sending request!');
        print(e.message);
      }
    }
    return res;
  }

 /* //Get response stream:
  Response<ResponseBody> rs;
  rs = await Dio().get<ResponseBody>(url,
  options: Options(responseType: ResponseType.stream),  // set responseType to `stream`
  );
  print(rs.data.stream); //response stream*/

 /* Sending FormData:

  var formData = FormData.fromMap({
    'name': 'wendux',
    'age': 25,
  });
  var response = await dio.post('/info', data: formData);*/


  /*Uploading multiple files to server by FormData:

  var formData = FormData.fromMap({
    'name': 'wendux',
    'age': 25,
    'file': await MultipartFile.fromFile('./text.txt', filename: 'upload.txt'),
    'files': [
      await MultipartFile.fromFile('./text1.txt', filename: 'text1.txt'),
      await MultipartFile.fromFile('./text2.txt', filename: 'text2.txt'),
    ]
  });
  var response = await dio.post('/info', data: formData);*/


 /* post raw data send
  var params =  {
    "item": "itemx",
    "options": [1,2,3],
  };

  Response response = await _dio.post(getAddToCartURL,
  options: Options(headers: {
  HttpHeaders.contentTypeHeader: "application/json",
  }),
  data: jsonEncode(params),
  );*/

/*//add header
  Dio dio = new Dio();
  dio.options.headers['content-Type'] = 'application/json';
  dio.options.headers["authorization"] = "token ${token}";
  var params = {'email': email,'password': pwd};
  response = await dio.post(url, data: params);*/
}