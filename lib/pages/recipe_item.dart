import 'package:block_pattern/bloc/get_recipe_bloc.dart';
import 'package:block_pattern/helper/gridview_height.dart';
import 'package:block_pattern/helper/helper.dart';
import 'package:block_pattern/model/recipe_item_response/Data.dart';
import 'package:block_pattern/model/recipe_item_response/RecipeItemResponse.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RecipeItem extends StatefulWidget{
  var auth_token="";
  var location_id="";
  var recipe_id="";
  RecipeItem(this.auth_token, this.location_id, this.recipe_id);

  @override
  State<RecipeItem> createState() => _RecipeItemState(auth_token,location_id,recipe_id);
}

class _RecipeItemState extends State<RecipeItem> {
  var auth_token="";
  var location_id="";
  var recipe_id="";
  _RecipeItemState(this.auth_token, this.location_id, this.recipe_id);

 // double _crossAxisSpacing = 8, _mainAxisSpacing = 12, _aspectRatio = 2;
 // int _crossAxisCount = 2;
  var _aspectRatio;

  var _crossAxisCount = 2;

  @override
  Widget build(BuildContext context) {
   // double screenWidth = MediaQuery.of(context).size.width;
    var _crossAxisSpacing = 8;
    var _screenWidth = MediaQuery.of(context).size.width;
    var _width = ( _screenWidth - ((_crossAxisCount - 1) * _crossAxisSpacing)) / _crossAxisCount;
    var cellHeight = 80;
     _aspectRatio = _width /cellHeight;


    if(auth_token.isNotEmpty && location_id.isNotEmpty && recipe_id.isNotEmpty){
      recipesBloc.getRecipeItem(auth_token,location_id,recipe_id);
    }else{
      Helper().showToast("Some Field is null value !");
    }

   return Scaffold(
     appBar: AppBar(title: Text("Recipe item"),),
     backgroundColor: Colors.red,
     body: StreamBuilder<RecipeItemResponse>(
         stream: recipesBloc.recipeItem.stream,
         builder: (context, AsyncSnapshot<RecipeItemResponse> snapshot) {
           if (snapshot.hasData) {
             /* if (snapshot.data!.error != null && snapshot.data!.error.length > 0) {
              return _buildErrorWidget(snapshot.data!.error);
            }*/

             return buildList(snapshot.data!);
           } else if (snapshot.hasError) {
             return Text(snapshot.error.toString());
           } else {
             return Center(child: CircularProgressIndicator());
           }
         }),

   );
  }
  Widget buildList(RecipeItemResponse response) {
    List<Data> arrayList=[];
    arrayList=response.data!;
    return arrayList==null ? Text("No Data Found!") :   GridView.builder(
        itemCount: arrayList.length,
       // gridDelegate:  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: _crossAxisCount,childAspectRatio: _aspectRatio),
        gridDelegate:
         SliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
          crossAxisCount: 3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
          height: 160.0,
    ),

        shrinkWrap: true,

        itemBuilder: (BuildContext context, int index) {

          return  Card(

              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,

              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Image.network(arrayList[index].media_url!,),
                        ListTile(
                          title: Text(arrayList[index].item_name!,style: TextStyle(fontFamily: 'Raleway'),),
                          subtitle: Text(arrayList[index].item_id!),
                          /*leading: Image.network(arrayList[index].media_url!,
                        fit: BoxFit.fill,),*/

                        )

                      ],

                    ),
                  ),
                ],

              ),
            );

        });
  }
}
