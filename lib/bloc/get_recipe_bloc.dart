import 'package:block_pattern/model/login_response.dart';
import 'package:block_pattern/model/recipe_item_response/RecipeItemResponse.dart';
import 'package:block_pattern/model/recipe_response.dart';
import 'package:block_pattern/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class RecipesListBloc{

  final RecipeRepository _movieRepository = RecipeRepository();
  final BehaviorSubject<RecipeResponse> _subject =
  BehaviorSubject<RecipeResponse>();

  final BehaviorSubject<LoginResponse> _login =
  BehaviorSubject<LoginResponse>();

  final BehaviorSubject<RecipeItemResponse> _recipeItem =
  BehaviorSubject<RecipeItemResponse>();

  getRecipes(String auth_token) async{
    RecipeResponse response=await _movieRepository.getRecipes(auth_token);
    _subject.sink.add(response);
  }
  getLogin(String userid,String pwd) async{
    LoginResponse? response=await _movieRepository.getLogin(userid,pwd);
    _login.sink.add(response!);
  }
  getRecipeItem(String auth_token,String location_id,String recipe_id) async{
    RecipeItemResponse? response=await _movieRepository.getRecipeItem(auth_token,location_id,recipe_id);
    _recipeItem.sink.add(response!);
  }
  dispose() {
    _subject.close();
    _login.close();
    _recipeItem.close();
  }
  BehaviorSubject<RecipeResponse> get subject => _subject;
  BehaviorSubject<LoginResponse> get login => _login;

  BehaviorSubject<RecipeItemResponse> get recipeItem => _recipeItem;
}
final recipesBloc = RecipesListBloc();