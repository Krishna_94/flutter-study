class Recipe{
  final String recipe_id;
  final String recipe_name;
  final String total_items;
  final String update_at;


  Recipe(this.recipe_id, this.recipe_name, this.total_items, this.update_at);

  Recipe.fromJson(Map<String, dynamic> json)
      : recipe_id = json["recipe_id"],
        recipe_name = json["recipe_name"],
        total_items = json["total_items"],
        update_at = json["update_at"]
       ;
}