import 'package:block_pattern/model/recipe.dart';

class RecipeResponse{
  final List<Recipe> recipes;
  final String error;

  RecipeResponse(this.recipes, this.error);

  RecipeResponse.fromJson(Map<String, dynamic> json)
      : recipes = (json["data"] as List)
      .map((i) => new Recipe.fromJson(i))
      .toList(),
        error = "";

  RecipeResponse.withError(String errorValue)
      : recipes = [],
        error = errorValue;
}