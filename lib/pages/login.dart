import 'dart:async';

import 'package:block_pattern/bloc/get_recipe_bloc.dart';
import 'package:block_pattern/helper/circular_indicator.dart';
import 'package:block_pattern/helper/circular_progressbar.dart';
import 'package:block_pattern/helper/connection_status_singleton.dart';
import 'package:block_pattern/helper/constants.dart';
import 'package:block_pattern/helper/helper.dart';
import 'package:block_pattern/helper/internet_connection.dart';
import 'package:block_pattern/helper/session_manager.dart';
import 'package:block_pattern/model/login_response.dart';
import 'package:block_pattern/pages/recipe_list_view.dart';
import 'package:block_pattern/repository/repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget{
  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final RecipeRepository _dioClient = RecipeRepository();
  String auth_token="";
  bool isLoading = false;
  TextEditingController userdController=TextEditingController();

  TextEditingController pwdController=TextEditingController();
  bool isOffline = false;

  InternetConnection i=InternetConnection();
  isAvailableInternet() async {
    i.check().then((intenet) {
      if (intenet != null && intenet) {
        setState(() {

        });
        return true;
      }
      else{
        setState(() {

        });
      return false;
      }
    });
  }
  @override
  Widget build(BuildContext context) {

   return Scaffold(
     appBar: AppBar(title: Text("Login Screen"),),
     backgroundColor: Colors.red,
     body: Padding(
       padding: const EdgeInsets.all(10.0),
       child: Column(
         children: [
           SizedBox(height: 20,),
           TextField(
             onChanged: (text){
               //on text change
               print("value: $text");

             },
             style: TextStyle(color: Colors.white),
             controller: userdController,

             decoration: InputDecoration(
                 prefixIcon: Icon(Icons.mail_outline,color: Colors.white,),
                 // hintText: "Enter Name",
                 labelText: "UserId",
                 labelStyle: TextStyle(color: Colors.white),
                 border: new OutlineInputBorder(
                   borderRadius: const BorderRadius.all(
                     const Radius.circular(0.0),
                   ),
                   borderSide: new BorderSide(
                     color: Colors.black,
                     width: 1.0,
                   ),
                 )
             ),
           ),
           SizedBox(height: 20,),
           TextField(
             onChanged: (text){
               //on text change
               print("value: $text");

             },
             style: TextStyle(color: Colors.white),
             controller: pwdController,

             decoration: InputDecoration(
                 prefixIcon: Icon(Icons.lock_open,color: Colors.white,), // for custom icon MyFlutterApp.account_box
                 // hintText: "Enter Name",
                 labelText: "Password",
                 labelStyle: TextStyle(color: Colors.white),
                 border: new OutlineInputBorder(
                   borderRadius: const BorderRadius.all(
                     const Radius.circular(0.0),
                   ),
                   borderSide: new BorderSide(
                     color: Colors.black,
                     width: 1.0,
                   ),
                 )
             ),
           ),
           SizedBox(height: 20,),
           isLoading ?CircularProgressIndicatorApp():
           ElevatedButton(

               style: TextButton.styleFrom(backgroundColor: Colors.white,
               ),
               onPressed: () async {

                 validation();



               },
               child: Text("Sign In",style: TextStyle(color: Colors.black),)
           ),

           //_obseverResponse()
         ],
       ),
     ),
   );
  }
  Widget _obseverResponse() {
    return Center(
        child:StreamBuilder<LoginResponse?>(
        stream: recipesBloc.login.stream,
        builder: (context, AsyncSnapshot<LoginResponse?> snapshot) {

            if (snapshot!=null && snapshot.hasData) {
              /* if (snapshot.data!.error != null && snapshot.data!.error.length > 0) {
              return _buildErrorWidget(snapshot.data!.error);
            }*/
              //_buildLoadingWidget();
              return listWidget(snapshot.data!);

            } else if (snapshot.hasError) {
              return _buildErrorWidget(snapshot.data!.message!);

            } else {
              return CircularProgressIndicatorApp();;
            }


    })
    );
  }
  Widget listWidget(LoginResponse data){
    Helper().showToast("" + data.message!);
    if(data.data!=null){
      auth_token=data.data!.authToken!;
  }
    if(data.success==true){
      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>RecipeListview(auth_token)));
    }

   /* Navigator
        .of(context)
        .pushReplacement(new MaterialPageRoute(builder: (BuildContext context) => RecipeListview(auth_token))
    );*/

   /* Navigator.pushReplacementNamed(context, '/recipePage',arguments: {"auth_token" :
    auth_token, "rollNo": 65210});*/
     return Container();
  }
  Widget _buildErrorWidget(String error) {
    Helper().showToast("" + error);
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            Text("Error occured: $error"),
          ],
        ));
  }
  Widget _buildLoadingWidget() {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 25.0,
              width: 25.0,
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                strokeWidth: 4.0,
              ),
            )
          ],
        ));
  }
  Future<void> validation() async {

    String userid=userdController.text.toString();
    String pwd=pwdController.text.toString();

    if(userid.isEmpty || userid==null){
      Helper().showToast("UserId Field is required !");


    }else if(pwd.isEmpty || pwd==null){
      Helper().showToast("Password Field is required !");

    }

    else{



      setState(() {
        isLoading = true;
      });



      InternetConnection internet=InternetConnection();
      internet.isAvailableInternet((isNetworkPresent) async {
        if(!isNetworkPresent){
          Helper().showToast("internet not available");
          setState(() {
            isLoading = false;
          });
        }

      });

      LoginResponse? retrievedUser = await _dioClient.getLogin2(userid,pwd);



       if(retrievedUser!.success==true){

         print("token: "+retrievedUser.data!.authToken!);
         auth_token=retrievedUser.data!.authToken!;
         setState(() {
           isLoading = false;
         });
        // pref.setBooleanValue("isLoggedIn",true);
       //  pref.setStringValue(Constants().auth_token,retrievedUser.data!.authToken!);
         start(retrievedUser.data!.authToken!);
         Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>RecipeListview(auth_token)));
         Helper().showToast(retrievedUser.message!);
       }else{
         setState(() {
           isLoading = false;
         });
         Helper().showToast(retrievedUser!.message!);
       }

     }
    //  recipesBloc..getLogin(userid,pwd);

    }


  }
start(String token) async {

  SessionManager.instance.setBooleanValue(Constants().isLoggedIn, true);
  //  await pref.setBooleanValue("isLoggedIn",true);
  SessionManager.instance.setStringValue(Constants().auth_token,token);
}
fetchPrefrence(bool isNetworkPresent) {
  if(isNetworkPresent){
    Helper().showToast("internet available");

  }else{
    Helper().showToast("internet not available");

  }
}
