class Data {
    String? item_id;
    String? item_name;
    String? media_type;
    String? media_url;
    String? update_at;

    Data({this.item_id, this.item_name, this.media_type, this.media_url, this.update_at});

    factory Data.fromJson(Map<String, dynamic> json) {
        return Data(
            item_id: json['item_id'],
            item_name: json['item_name'],
            media_type: json['media_type'],
            media_url: json['media_url'],
            update_at: json['update_at'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['item_id'] = this.item_id;
        data['item_name'] = this.item_name;
        data['media_type'] = this.media_type;
        data['media_url'] = this.media_url;
        data['update_at'] = this.update_at;
        return data;
    }
}