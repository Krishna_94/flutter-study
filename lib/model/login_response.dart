class LoginResponse {
     bool? success;
     Data? data;
     String? message;

    LoginResponse({required this.success, required this.data, required this.message});

    LoginResponse.withError(String errorValue)
        :message = errorValue;

    LoginResponse.fromJson(Map<String, dynamic> json) {
        success = json['success'];
        if(json['data'] !=null){
          data = (json['data'] != null ? new Data.fromJson(json['data']) : null)!;
        }
        message = json['message'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['success'] = this.success;
        if (this.data != null) {
            data['data'] = this.data!.toJson();
        }
        data['message'] = this.message;
        return data;
    }
}

class Data {
    String? userId;
    String? firstName;
    String? lastName;
   String? email;
   String? authToken;
   String? secretCode;
   String? deviceToken;
   String? userType;
   String? status;
   String? currentLocation;
   List<Location>? location;

    Data(
        {required this.userId,
            required this.firstName,
            required this.lastName,
            required this.email,
            required this.authToken,
            required this.secretCode,
            required this.deviceToken,
            required this.userType,
            required this.status,
            required this.currentLocation,
            required this.location});

    Data.fromJson(Map<String, dynamic> json) {
        userId = json['user_id'];
        firstName = json['first_name'];
        lastName = json['last_name'];
        email = json['email'];
        authToken = json['auth_token'];
        secretCode = json['secret_code'];
        deviceToken = json['device_token'];
        userType = json['user_type'];
        status = json['status'];
        currentLocation = json['current_location'];
        if (json['location'] != null) {
            location = [];
            json['location'].forEach((v) {
                location!.add(new Location.fromJson(v));
            });
        }
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['user_id'] = this.userId;
        data['first_name'] = this.firstName;
        data['last_name'] = this.lastName;
        data['email'] = this.email;
        data['auth_token'] = this.authToken;
        data['secret_code'] = this.secretCode;
        data['device_token'] = this.deviceToken;
        data['user_type'] = this.userType;
        data['status'] = this.status;
        data['current_location'] = this.currentLocation;
        if (this.location != null) {
            data['location'] = this.location!.map((v) => v.toJson()).toList();
        }
        return data;
    }
}

class Location {
   String? locationId;
   String? locationName;

    Location({required this.locationId, required this.locationName});

    Location.fromJson(Map<String, dynamic> json) {
        locationId = json['location_id'];
        locationName = json['location_name'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['location_id'] = this.locationId;
        data['location_name'] = this.locationName;
        return data;
    }
}