import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {

  SessionManager._privateConstructor();

  static final SessionManager instance =
  SessionManager._privateConstructor();
//set string data into shared preferences like this
    Future<void> setStringValue(String key,String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

//get string value from shared preferences
   Future<String?> getStringValue(String key) async {
     final SharedPreferences prefs = await SharedPreferences.getInstance();
     return prefs.getString(key) ?? null;

  }

  //set integer data into shared preferences like this
   Future<void> setIntValue(String key,int value) async {
     final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value);
  }

//get integer value from shared preferences
   Future<int?> getIntValue(String key) async {
     final SharedPreferences prefs = await SharedPreferences.getInstance();
     return prefs.getInt(key) ?? null;
  }
  //set boolean value from shared preferences
   Future<void> setBooleanValue(String key,bool value) async {
     final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
  }

//get boolean value from shared preferences
   Future<bool?> getBooleanValue(String key) async {
     final SharedPreferences prefs = await SharedPreferences.getInstance();
    bool auth_token;
     return prefs.getBool(key) ?? null;
  }
   Future<void> alldatadelete() async {
     final SharedPreferences prefs = await SharedPreferences.getInstance();
     prefs.clear();


  }
   Future<void> specficdatadelete(String key) async {
     final SharedPreferences prefs = await SharedPreferences.getInstance();
     prefs.remove(key);


  }

}