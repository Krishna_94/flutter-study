import 'package:block_pattern/widgets/build_button_column.dart';
import 'package:block_pattern/widgets/text_section.dart';
import 'package:block_pattern/widgets/title_section.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailsPage extends StatelessWidget{
 static Color? color;
  @override
  Widget build(BuildContext context) {
    color = Theme.of(context).primaryColor;
  return MaterialApp(
    title: 'Deatils Page',
    theme: Theme.of(context),
    color: Colors.amberAccent,
    home: Scaffold(
      appBar: AppBar(title: Text("Deatils Screen"),),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Image.asset(
            'assets/images/login_image.png',
                            width: 600,
                            height: 240,
                            fit: BoxFit.cover,
          ),
          titleSection,
          buttonSection,
          textSection
        ],

      ),
    ),

  );
  }

}