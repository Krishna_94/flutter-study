import 'package:block_pattern/pages/details_page.dart';
import 'package:block_pattern/style/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget buttonSection =Row(

  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  children: [
    _buildButtonColumn(Icons.call,'CALL'),
    _buildButtonColumn(Icons.near_me,'ROUTE'),
    _buildButtonColumn(Icons.share,'SHARE'),
  ],

);
Column _buildButtonColumn( IconData icon, String label){

  return Column(
    mainAxisSize:  MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Icon(icon,color:  DetailsPage.color,),
      Container(
        margin: const EdgeInsets.only(top: 8.0),
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: DetailsPage.color
          ),
        ),
      )

    ],

  );
}
