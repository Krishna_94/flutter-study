
import 'package:block_pattern/helper/helper.dart';
import 'package:block_pattern/helper/session_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Signup extends StatelessWidget{
  TextEditingController nameController=TextEditingController();
  TextEditingController emailController=TextEditingController();
  TextEditingController pwdController=TextEditingController();
  TextEditingController confirmpwdController=TextEditingController();
  TextEditingController genderController=TextEditingController();
  TextEditingController dobController=TextEditingController();
  BuildContext? mContext=null;


  @override
  Widget build(BuildContext context) {

    mContext=context;
   return Scaffold(
     appBar: AppBar(title: Text("Signup screen"),),
     backgroundColor: Colors.red,
     body: SingleChildScrollView(


       child: Padding(
         padding: const EdgeInsets.all(20.0),
         child: Column(

           children: [

             SizedBox(height: 20.0,width: 10.0,),
             TextField(

               style: TextStyle(color: Colors.white),

               maxLines: 1,
               maxLength: 20,

               controller: nameController,
               decoration: InputDecoration(
                // hintText: "Enter Name",

                   prefixIcon: Image.asset("assets/icons/name.png",height: 24,width: 24,color: Colors.white,),
                  labelText: "Name",
                 labelStyle: TextStyle(color: Colors.white),
                 border: new OutlineInputBorder(
                   borderRadius: const BorderRadius.all(
                     const Radius.circular(0.0),
                   ),
                   borderSide: new BorderSide(
                     color: Colors.black,
                     width: 1.0,
                   ),
                 )


               ),

             ),
             SizedBox(height: 20,),
             TextField(
               onChanged: (text){
                 //on text change
                 print("value: $text");

               },
               style: TextStyle(color: Colors.white),
               controller: emailController,

               decoration: InputDecoration(
                 prefixIcon: Icon(Icons.mail_outline,color: Colors.white,),
                 // hintText: "Enter Name",
                   labelText: "Email",
                 labelStyle: TextStyle(color: Colors.white),
                   border: new OutlineInputBorder(
                     borderRadius: const BorderRadius.all(
                       const Radius.circular(0.0),
                     ),
                     borderSide: new BorderSide(
                       color: Colors.black,
                       width: 1.0,
                     ),
                   )
               ),
             ),

             Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: [
                 new Flexible(
                   child: new TextField(
                     maxLines:1,
                     controller: genderController,
                       style: TextStyle(color: Colors.white),
                       decoration: InputDecoration(

                           labelText: "Gender",
                           labelStyle: TextStyle(color: Colors.white),
                           contentPadding: EdgeInsets.fromLTRB(10,0,0,0)
                       )
                   ),
                 ),
                 SizedBox(width: 10.0,),
                 new Flexible(
                   child: new TextField(
                     keyboardType: TextInputType.number,
                       textAlign: TextAlign.right,
                     controller: dobController,
                       style: TextStyle(color: Colors.white),
                       decoration: InputDecoration(
                           labelText: "D.O.B",

                           labelStyle: TextStyle(color: Colors.white),
                           contentPadding: EdgeInsets.all(10)
                       )
                   ),
                 ),

               ],
             ),
             TextField(
               style: TextStyle(color: Colors.white),
               controller: pwdController,

               decoration: InputDecoration(
                 // hintText: "Enter Name",
                   labelText: "Password",
                 labelStyle: TextStyle(color: Colors.white),
               ),
             ),
             TextField(

               style: TextStyle(color: Colors.white),
               controller: confirmpwdController,

               decoration: InputDecoration(
                 // hintText: "Enter Name",
                 contentPadding: const EdgeInsets.all(20.0),
                   labelText: "Confirm Password",
                 labelStyle: TextStyle(color: Colors.white),

               ),
             ),
             SizedBox(
               height: 20,
             ),

             ElevatedButton(

               style: TextButton.styleFrom(backgroundColor: Colors.white,
               ),
                 onPressed: (){
                   validation();
                 },
                 child: Text("Submit",style: TextStyle(color: Colors.black),))
           ],
         ),
       ),

     ),
   );
  }
  Future<void> validation() async {

   // var s=ListViewDialog();
   // s.show_dialog2(mContext!);
   // SessionManager pref=SessionManager();
    try {
      //await pref.alldatadelete();


      //get data
     // var  value=await SessionManager.getStringValue("name");
      //int  value2=await SessionManager.getIntValue("dob");
     // bool  value3=await pref.getBooleanValue("status");
    //  nameController.text=value;
     /* if(value2!=null){
        dobController.text=value2.toString();
      }*/

     // print(value);
     // print("${value2}");
    // print("${value3}");

    } on Exception catch (exception) {
      print(exception);
    } catch (error) {
      print(error);

    }

   /* Helper().ShowDialog(mContext!);
    Timer(Duration(seconds: 3), () {
      Navigator.pop(mContext!);
    });*/
    String name=nameController.text;
    String email=emailController.text.toString();
    String pwd=pwdController.text.toString();
    String confirm=confirmpwdController.text.toString();
    String gender=genderController.text.toString();
    String dob=dobController.text.toString();
    if(name.isEmpty || name==null){
      Helper().showToast("Name Field is required !");

    }else if(email.isEmpty || email==null){
      Helper().showToast("email Field is required !");
    }
    else if(gender.isEmpty || gender==null){
      Helper().showToast("gender Field is required !");
    }
    else if(dob.isEmpty || dob==null){
      Helper().showToast("dob Field is required !");
    }
    else if(pwd.isEmpty || pwd==null){
      Helper().showToast("pwd Field is required !");
    }
    else if(confirm.isEmpty || confirm==null){
      Helper().showToast("confirm password Field is required !");
    }
    else if(pwd!=confirm){
      Helper().showToast("confirm password not matched !");
    }
    else{
     // SessionManager.setStringValue("name",name);
    //  SessionManager.setIntValue("dob",int.parse(dob));

      Helper().showToast("all ok");
    }


  }



}