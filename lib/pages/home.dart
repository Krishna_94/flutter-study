import 'package:block_pattern/helper/custom_app_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(title: Text("Home Screen"),),
    backgroundColor: Colors.red,
    body: Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Text("Deliver features faster",textAlign: TextAlign.center,),
            ),
            Expanded(
              child: Text("Craft beautiful UIs",textAlign: TextAlign.center,),
            ),

            Expanded(
              child: Image(image: AssetImage(  'assets/images/logo.png'),height: 50,),
            )
          ],

        ),
        SizedBox(height: 20,),
        Row(
          textDirection:  TextDirection.ltr,
          children: [
            const FlutterLogo(),

            Expanded(
              child: Text("Deliver features faster",textAlign: TextAlign.center),
            ),
            Expanded(child: Text("Flutter's hot reload helps you quickly and easily experiment, build UIs, add features, and fix bug faster. Experience sub-second reload times, without losing state, on emulators, simulators, and hardware for iOS and Android.")),

            Expanded(

              child: Image(image: AssetImage(  'assets/images/logo.png'),height: 50,),
            )
          ],

        ),
        SizedBox(height: 20,),
        Align(

          alignment: Alignment.topCenter,
          child: Container(
            height: 120,
            width: 120,
            color: Colors.blue[50],
            child: Text("Krishna"),

            alignment: Alignment.topRight,
          ),
        ),
        SizedBox(height: 20,),
       const Text.rich(
           TextSpan(
             text: 'Hello',
             children: [
               TextSpan(
                 text: ' beautiful ',style: TextStyle(fontStyle: FontStyle.italic)
               ),
               TextSpan(
                 text: ' world ',style: TextStyle(fontWeight: FontWeight.bold)
               ),
               WidgetSpan(
                 child: Padding(
                   padding: const EdgeInsets.symmetric(horizontal: 2.0),
                   child: Icon(MyFlutterApp.audiotrack),
                 ),
               ),
             ]
           )
       ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: const <Widget>[
            Icon(
              Icons.favorite,
              color: Colors.pink,
              size: 24.0,
              semanticLabel: 'Text to announce in accessibility modes',
            ),
            Icon(
              Icons.audiotrack,
              color: Colors.green,
              size: 30.0,
            ),
            Icon(
              MyFlutterApp.audiotrack,
              color: Colors.blue,
              size: 36.0,
            ),
          ],
        )
      ],

    ),
  );
  }

}