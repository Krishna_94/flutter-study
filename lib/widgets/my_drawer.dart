import 'package:block_pattern/helper/session_manager.dart';
import 'package:block_pattern/pages/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class MyDrawer extends StatelessWidget{

  String imgurl="https://images.unsplash.com/photo-1503327431567-3ab5e6e79140?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=80";
  @override
  Widget build(BuildContext context) {
   return Drawer(
     child: Container(
       color: Colors.deepPurple,
       child: ListView(
         padding: EdgeInsets.zero,
         children: [
         DrawerHeader(
           padding: EdgeInsets.zero,
             child: UserAccountsDrawerHeader(
               margin: EdgeInsets.zero,
               decoration: BoxDecoration(color: Colors.deepPurple),
               accountName: Text("Krishna Kumar"),
             accountEmail: Text("krishna.techexactly@gmail.com"),
               currentAccountPicture: CircleAvatar(
                 radius: 50.0,
                 backgroundImage: NetworkImage(imgurl),
               ),
             )
         ),
           ListTile(
             leading: Icon(CupertinoIcons.home,
             color: Colors.white,),
             title: Text("Home",style: TextStyle(color: Colors.white
             ),
             ),

           ),
           ListTile(
             leading: Icon(CupertinoIcons.profile_circled,
               color: Colors.white,),
             title: Text("Profile",style: TextStyle(color: Colors.white
             ),
             ),

           ),
           ListTile(
             leading: Icon(CupertinoIcons.mail,
               color: Colors.white,),
             title: Text("Email Me",style: TextStyle(color: Colors.white
             ),
             ),

           ),
           ListTile(
             leading: Icon(CupertinoIcons.lock_open,
               color: Colors.white,),
             title: Text("Logout",style: TextStyle(color: Colors.white
             ),
             ),
             onTap: () async {
               SessionManager.instance.alldatadelete();
               Navigator.of(context)
                   .pushReplacement(MaterialPageRoute(builder: (_) => Login()));
             },


           ),
       ],),
     ),
   );
  }

}