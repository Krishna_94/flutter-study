import 'dart:async';

import 'package:connectivity/connectivity.dart';

import 'helper.dart';

class InternetConnection{

/*
  Future<bool> isAvailableInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    Helper().showToast("Internet Connection must be required");
    return false;
  }*/
  Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }
 /* bool isAvailableInternet(){
    bool status=false;
    check().then((intenet) {
      if (intenet != null && intenet) {
        return status;
      }
      else{
        Helper().showToast("Internet Connection Required!");
        status=false;
        return status;
      }
    });
    return status;
  }*/
  dynamic isAvailableInternet(Function func) {
    check().then((intenet) {
      if (intenet != null && intenet) {
        func(true);
      }
      else{
        func(false);
      }
    });
  }


}