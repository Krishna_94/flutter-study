import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

 class Helper {
   static final Helper _singleton = Helper._internal();

   factory Helper() {
     return _singleton;
   }

   Helper._internal();

  void showToast(String message){
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
     fontSize: 16.0,
     textColor: Colors.white


    );
  }
   void showAlertProgressDialog(BuildContext context){
     AlertDialog alert=AlertDialog(
       content: new Row(
         children: [
           CircularProgressIndicator(),
           Container(margin: EdgeInsets.only(left: 5),child:Text("Loading" )),
         ],),
     );
     showDialog(barrierDismissible: true,
       context:context,
       builder:(BuildContext context){
         return alert;
       },
     );
   }

  void showCircularProgrees(BuildContext context){
     AlertDialog alert=AlertDialog(
       content: new Row(
         children: [
           CircularProgressIndicator(),
           //Container(margin: EdgeInsets.only(left: 5),child:Text("Loading" )),
         ],),
     );
     showDialog(barrierDismissible: true,
       context:context,
       builder:(BuildContext context){
         return alert;
       },
     );
   }
    ShowDialog(BuildContext context) {
     return showDialog(
         context: context,
         barrierDismissible: true,
         builder: (BuildContext context) {
           return Center(
             child: CircularProgressIndicator(color: Colors.white,),
           );
         });

     //hide  dialog
     // Navigator.pop(mContext!);
   }
  void showAlertDialog(BuildContext context) {

     // set up the buttons
     Widget cancelButton = TextButton(
       child: Text("Cancel"),
       onPressed:  () {
         Navigator.of(context).pop(); // dismiss dialog
       },
     );
     Widget continueButton = TextButton(
       child: Text("Continue"),
       onPressed:  () {
         Helper().showToast("Continue");
         Navigator.of(context).pop(); // dismiss dialog
       },
     );

     // set up the AlertDialog
     AlertDialog alert = AlertDialog(
       title: Text("AlertDialog"),
       content: Text("Would you like to continue learning how to use Flutter alerts?"),
       actions: [
         cancelButton,
         continueButton,
       ],
     );

     // show the dialog
     showDialog(
       context: context,
       builder: (BuildContext context) {
         return alert;
       },
     );
   }
   showAlertDialog2(BuildContext context) {

     // set up the buttons
     Widget remindButton = TextButton(
       child: Text("Remind me later"),
       onPressed:  () {
         Navigator.of(context).pop(); // dismiss dialog
       },
     );
     Widget cancelButton = TextButton(
       child: Text("Cancel"),
       onPressed:  () {
         Navigator.of(context).pop(); // dismiss dialog
       },
     );
     Widget launchButton = TextButton(
       child: Text("Launch missile"),
       onPressed:  () {
         Navigator.of(context).pop(); // dismiss dialog
       },
     );

     // set up the AlertDialog
     AlertDialog alert = AlertDialog(
       title: Text("Notice"),
       content: Text("Launching this missile will destroy the entire universe. Is this what you intended to do?"),
       actions: [
         remindButton,
         cancelButton,
         launchButton,
       ],
     );

     // show the dialog
     showDialog(
       context: context,
       builder: (BuildContext context) {
         return alert;
       },
     );
   }
}