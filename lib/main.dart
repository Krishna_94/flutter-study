
import 'dart:async';

import 'package:block_pattern/helper/constants.dart';
import 'package:block_pattern/pages/details_page.dart';
import 'package:block_pattern/pages/home.dart';
import 'package:block_pattern/pages/login.dart';
import 'package:block_pattern/pages/recipe_list_view.dart';
import 'package:block_pattern/pages/signup_page.dart';
import 'package:flutter/material.dart';

import 'helper/connection_status_singleton.dart';
import 'helper/session_manager.dart';

void main() {
  ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
  connectionStatus.initialize();
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SplashScreen(),//SplashScreen()
     /* routes: {
        '/' : (BuildContext context)=>SplashScreen(),
        '/login' : (BuildContext context)=>Login(),
        '/recipePage' : (BuildContext context)=>RecipeListview(""),
      },*/
    );
  }
}
class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String auth_token="";

  @override
   initState() {
    // TODO: implement initState
    super.initState();

    session();

  }
 void session()  {
   String? auth_token=null;
   bool? isLoggedIn=null;
    var isLogged = SessionManager.instance.getBooleanValue("isLoggedIn");

   SessionManager.instance
       .getBooleanValue(Constants().isLoggedIn)
       .then((value) => setState(() {
     var isLogged = value;
      isLoggedIn = (isLogged == null) ? false : isLogged;
   }));
    SessionManager.instance
        .getStringValue(Constants().auth_token)
        .then((value) => setState(() {
       auth_token = value;
    }));
   // auth_token = SessionManager.instance.getStringValue(Constants().auth_token);
    // var  isLoggedIn=await pref.getBooleanValue(Constants.isLoggedIn);
    Timer(Duration(seconds: 3), () {
      if (isLoggedIn == true) {
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(
            builder: (_) => RecipeListview(auth_token!))); //LoginPage()
      } else {
        Navigator.of(context)
            .pushReplacement(
            MaterialPageRoute(builder: (_) => Login())); //LoginPage()
      }


      //  Navigator.pushReplacementNamed(context, '/login');
    });

  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.deepPurple[700],
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // logo here
            Image.asset(
              'assets/images/logo.png',
              height: 120,

            ),
            SizedBox(
              height: 20,
            ),
            CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            )
          ],
        ),
      ),
    );

  }
}
