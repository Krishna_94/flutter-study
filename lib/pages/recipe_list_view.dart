import 'dart:async';

import 'package:block_pattern/bloc/get_recipe_bloc.dart';
import 'package:block_pattern/helper/helper.dart';
import 'package:block_pattern/helper/internet_connection.dart';
import 'package:block_pattern/model/recipe.dart';
import 'package:block_pattern/model/recipe_response.dart';
import 'package:block_pattern/pages/details_page.dart';
import 'package:block_pattern/pages/recipe_item.dart';
import 'package:block_pattern/widgets/my_drawer.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RecipeListview extends StatefulWidget{
  var auth_token="";

  RecipeListview(this.auth_token);
//  RecipeListview(String s);


  @override
  State<RecipeListview> createState() => _RecipeListviewState(auth_token);


}

class _RecipeListviewState extends State<RecipeListview> {
  var auth_token="";
  var location_id="";
  var recipe_id="";
  _RecipeListviewState(this.auth_token);

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    recipesBloc..getRecipes(auth_token);
  return Scaffold(
    appBar: AppBar(title: Text("Recipe List View"),),
    drawer: MyDrawer(),
    backgroundColor: Colors.red,
    body: StreamBuilder<RecipeResponse>(
        stream: recipesBloc.subject.stream,
        builder: (context, AsyncSnapshot<RecipeResponse> snapshot) {
          if (snapshot.hasData) {
           /* if (snapshot.data!.error != null && snapshot.data!.error.length > 0) {
              return _buildErrorWidget(snapshot.data!.error);
            }*/

            return listWidget(snapshot.data!);
          } else if (snapshot.hasError) {
            return _buildErrorWidget(snapshot.data!.error);
          } else {
            return _buildLoadingWidget();
          }
        })
  );

  }



  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Error occured: $error"),
          ],
        ));
  }

  Widget _buildLoadingWidget() {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 25.0,
              width: 25.0,
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                strokeWidth: 4.0,
              ),
            )
          ],
        ));
  }

  Widget listWidget(RecipeResponse data){
    String imgurl="https://images.unsplash.com/photo-1503327431567-3ab5e6e79140?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=80";
    final List<Recipe> recipeList=  data.recipes.toList();
   return recipeList.isEmpty ? Center(child: Text('No Data Found !')) :ListView.builder(
        itemCount: recipeList.length,
        itemBuilder: (BuildContext context,int index){
          return Card(

            child: Row(
             // textDirection: TextDirection.rtl,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                 Expanded(
                   child: Column(
                    children: [
                      ListTile(
                        onTap: (){
                          location_id="1";
                          recipe_id=recipeList[index].recipe_id;
                          Navigator.of(context).push(MaterialPageRoute(builder: (context)=>RecipeItem(auth_token,location_id,recipe_id)));
                        },

                        leading: Image.network(imgurl,
                          height: 50,),
                        /*trailing: Text("GFG kk",
                          style: TextStyle(
                              color: Colors.black,fontSize: 15),),*/
                        title:Text(recipeList[index].recipe_name ,
                            style: TextStyle(
                                color: Colors.black,fontSize: 15)
                        ),
                        subtitle: Text(recipeList[index].total_items),

                      ),


                    ],
                ),
                 ),
                Expanded(
                    child: Align(
                      alignment: Alignment.centerLeft,
                        child: Text('category',
                          textAlign: TextAlign.start,))),
                Expanded(
                  child: Align(
                      alignment: Alignment.topRight,
                      child: TextButton(
                          onPressed: (){
                            InternetConnection internet=InternetConnection();
                            internet.isAvailableInternet((isNetworkPresent){
                              if(isNetworkPresent){
                                Helper().showToast("internet available");
                                Navigator.of(context).push(MaterialPageRoute(builder: (context)=>DetailsPage()));

                              }else{
                                Helper().showToast("internet not available");

                              }

                            });


                          },
                          child: Text("view",textAlign: TextAlign.start,)
                      )
                  ),
                )
              ],

            ),
          );
        }
    );
  }


}

